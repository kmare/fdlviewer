import processing.serial.*;
import processing.opengl.*;
import toxi.geom.*;
import toxi.processing.*;

// NOTE: requires ToxicLibs to be installed in order to run properly.
// 1. Download from http://toxiclibs.org/downloads
// 2. Extract into [userdir]/Processing/libraries
//    (location may be different on Mac/Linux)
// 3. Run and bask in awesomeness

ToxiclibsSupport gfx;

Serial port;                         // The serial port
int serialCount = 0;                 // current packet byte position
int synced = 0;
int interval = 0;

float[] q = new float[4];
Quaternion quat = new Quaternion(1, 0, 0, 0);

float[] gravity = new float[3];
float[] euler = new float[3];
float[] ypr = new float[4];
float[] gps = new float[4];

String rs = "";
PShape s;
PFont font;

void setup() {
    size(1920, 1080, P3D);
    gfx = new ToxiclibsSupport(this);

    // setup lights and antialiasing
    lights();
    smooth();
  
    // display serial port list
    println(Serial.list());

    // get the first available port (use EITHER this OR the specific port code below)
    String portName = Serial.list()[0];
   
    // open the serial port
    // port = new Serial(this, portName, 115200);
    // port = new Serial(this, portName, 57600);
    // port = new Serial(this, "/dev/ttyUSB0", 57600);
    
    port = new Serial(this, "/dev/ttyUSB0", 115200);
    // port = new Serial(this, "/dev/rfcomm0", 115200);
    
    s = loadShape("plane.obj");
    s.scale(10);
    
    font = loadFont("ArialUnicodeMS-48.vlw"); 
    textFont(font, 28);
    
    ypr[0] = ypr[1] = ypr[2] = ypr[3] = 0;
    gps[0] = gps[1] = gps[2] = gps[3] = 0;
}

void draw() {
    // black background
    background(0);
    
    textFont(font, 48);
    fill(0, 255, 0);
    text("Flight Data Logger", 100, 150);
    
    fill(255, 0, 0);
    textFont(font, 28);
    text("Quaternion:\n  qw: " + q[0] + "\n  qx: " + q[1] + "\n  qy: " + q[2] + "\n  qz: " + q[3], 100, 300);
    
    text("Yaw/Pitch/Roll:\n  yaw: " + ypr[0] + "\n  pitch: " + ypr[1] + "\n  roll: " + ypr[2] + "\n  yaw rate: " + ypr[3], 100, 500);
    
    text("GPS:\n  latitude: " + gps[0] + "\n  longitude: " + gps[1] + "\n  speed: " + gps[2] + "\n  time: " + gps[3], 100, 700);
    
    
    // translate everything to the middle of the viewport
    pushMatrix();
    translate(width / 2, height / 2);

    // toxiclibs direct angle/axis rotation from quaternion (NO gimbal lock!)
    // (axis order [1, 3, 2] and inversion [-1, +1, +1] is a consequence of
    // different coordinate system orientation assumptions between Processing
    // and InvenSense DMP)
    float[] axis = quat.toAxisAngle();
    rotate(axis[0], -axis[1], axis[3], axis[2]);

    // draw airplane
    shape(s);
    
    
    popMatrix();
}

void serialEvent(Serial port) {
    interval = millis();
    
    String myString = port.readStringUntil('\n');
    
    if (myString != null) {
      myString = trim(myString);
      // println(myString);
      
      String sensors[] = split(myString, ',');
      if (sensors.length > 3) {
        q[0] = float(sensors[0]);
        q[1] = float(sensors[1]);
        q[2] = float(sensors[2]);
        q[3] = float(sensors[3]);
        
        for (int i = 0; i < 4; i++) 
          if (q[i] >= 2) 
            q[i] = -4 + q[i];
        quat.set(q[3], q[0], q[1], q[2]);
        
        if (sensors.length > 6) {
          ypr[0] = float(sensors[4]);
          ypr[1] = float(sensors[5]);
          ypr[2] = float(sensors[6]);
          ypr[3] = float(sensors[7]);
        }
        
        if (sensors.length > 9) {
          gps[0] = float(sensors[8]);
          gps[1] = float(sensors[9]);
          gps[2] = float(sensors[10]);
          gps[3] = float(sensors[11]);
        }
      }
    }
    
}
